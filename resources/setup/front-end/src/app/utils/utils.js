export function parseUrl() {
	
	debugger; // <---
	const url = window.location;
    const query = url.href.split('?')[1] || '';
    const delimiter = '&';

    return query
    .split(delimiter)
    .map(p => p.split('='))
         .reduce((acc, kv) => 
            {acc[kv[0]] = kv[1];
                return acc}, {});
}