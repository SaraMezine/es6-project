import scoretemplate from "./score.component.html";
import './score.component.scss';
import { Component } from '../../utils/component';

export class ScoreComponent extends Component{

        constructor(){
        super('score');
         let params = parseUrl();
         this.name = params.name;
         this.size = parseInt(params.size);
         this.time = parseInt(params.time); 
     }

     init() {
        document.getElementById('name').innerText = this.name;
        document.getElementById('size').innerText = this.size;
        document.getElementById('time').innerText = this.time;
    }

    getTemplate() {
        return scoretemplate;
    }

        
    }


    function parseUrl() {
        const url = window.location;
        const query = url.href.split('?')[1] || '';
        const delimiter = '&';
        let result = {};

        let parts = query.split(delimiter);

        for (let i in parts) {
            let item = parts[i];
            let kv = item.split('=');
            result[kv[0]] = kv[1];
        }

        return result;
    }
    